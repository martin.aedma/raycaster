﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;

namespace RayCaster.N2942
{
    // Class for interacting with SQLite Database
    public static class Dbase
    {
        // Program uses single file for database, filename(Shapes.db) is configured in App.config
        private readonly static string filename;
        
        static Dbase()
        {
            filename = FinalProjectGUI.Properties.Settings.Default.dbase;
        }

        //Method for reading database table info into program. Each map has a it's own table. Method is called with tablename.
        // Each row in table has four integers which define starting and end point of a line
        public static List<Line> ReadFromSQLite(string tableName)
        {
            if (System.IO.File.Exists(filename))
            {
                SQLiteConnection connection = new SQLiteConnection($"Data source = {filename}; Version=3");
                connection.Open();
                SQLiteCommand cmd = new SQLiteCommand($"SELECT * FROM {tableName}", connection);
                SQLiteDataReader rdr = cmd.ExecuteReader();
                DataTable dataTable = new DataTable();
                dataTable.Load(rdr);
                rdr.Close();

                List<Line> lines = new List<Line>();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    DataRow row = dataTable.Rows[i];
                    Point start = new Point();
                    Point end = new Point();
                    start.X = (int)row[1];
                    start.Y = (int)row[2];
                    end.X = (int)row[3];
                    end.Y = (int)row[4];

                    PointF[] points = new PointF[2];
                    points[0] = start;
                    points[1] = end;

                    Line line1 = new Line() { Points = points };
                    lines.Add(line1);
                }
                return lines;
            }
            else
            {
                throw new System.IO.FileNotFoundException("File not found");
            }
        }

        //Method for saving maps into database, a new table is created
        public static void WriteToSqlite(List<Point[]> structures)
        {
            SQLiteConnection connection = new SQLiteConnection($"Data source = {filename}; Version=3");
            connection.Open();
            string num = "Usermap" + (Dbase.GetTableNames().Count + 1).ToString();
            bool test = true;
            Console.WriteLine(num);

            try
            {
                SQLiteCommand cmd = new SQLiteCommand($"create table {num} (ID INT PRIMARY KEY NOT NULL, startX int, startY int, endX int, endY int)", connection);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                test = false;
                List<string> tables = Dbase.GetTableNames();
                if (tables.Contains("usermap"))
                {
                    SQLiteCommand c = new SQLiteCommand($"drop table usermap", connection);
                    c.ExecuteNonQuery();
                    SQLiteCommand cmd = new SQLiteCommand($"create table usermap (ID INT PRIMARY KEY NOT NULL, startX int, startY int, endX int, endY int)", connection);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    SQLiteCommand cmd = new SQLiteCommand($"create table usermap (ID INT PRIMARY KEY NOT NULL, startX int, startY int, endX int, endY int)", connection);
                    cmd.ExecuteNonQuery();
                }
            }

            int i = 1;
            string name;

            if (test == true)
            {
                name = num;
            }
            else
            {
                name = "usermap";
            }

            //Each line in Map has 2 points, start and end, each of those has X and Y value. Alltogether 4 integers values to define a line

            foreach (Point[] points in structures)
            {
                SQLiteCommand command = new SQLiteCommand($"insert into {name} (ID, startX, startY, endX, endY) values ({i}, {points[0].X},{points[0].Y},{points[1].X},{points[1].Y})", connection);
                command.ExecuteNonQuery();
                i++;
            }

            connection.Close();
            System.Windows.MessageBox.Show($"Map Saved - {name}");
        }

        //Method for displaying every table name in database, result is displayed when user enters Load menu
        public static List<string> GetTableNames()
        {
            SQLiteConnection connection = new SQLiteConnection($"Data source = {filename}; Version=3");
            connection.Open();
            SQLiteCommand cmd = new SQLiteCommand($"select name from sqlite_master where type='table' order by name", connection);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(rdr);
            rdr.Close();

            List<string> tables = new List<string>();

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {

                string name = (string)dataTable.Rows[i][0];
                tables.Add(name);
            }
            return tables;
        }

        // Method for removing a map = table from database
        public static void RemoveMap(string tableName)
        {
            if (tableName == "lines")
            {
                System.Windows.MessageBox.Show("Lines is default Map, remove failed");
                return;
            }

            SQLiteConnection connection = new SQLiteConnection($"Data source = {filename}; Version=3");
            connection.Open();

            try
            {
                SQLiteCommand cmd = new SQLiteCommand($"drop table {tableName}", connection);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                System.Windows.MessageBox.Show($"Unable to remove - {tableName}");
            }
            connection.Close();
        }
    }
}
