﻿using System.Windows;

namespace RayCaster.N2942
{
    //Line class is used to Save and Load maps and to create visibility polygon. Each line has start and end points.
    public class Line
    {
        public System.Drawing.PointF[] Points { get; set; }
        public double Angle { get; set; }

        public Line()
        {
            Points = new System.Drawing.PointF[2];
        }

        //When all lines are drawn and we want to display polygon view. Each line endpoint has to be connected to it's
        //neighbouring points. This method calculates angel's for each line, we can then order them in a list by angle
        //each line has realtive to frame (straight line going from player position horizontally to right edge of screen)
        //so that we can draw easily polygon view
        public void CalculateAngle(System.Drawing.PointF[] frame)
        {
            Vector a = new Vector() { X = Points[1].X - Points[0].X, Y = Points[1].Y - Points[0].Y };
            Vector b = new Vector() { X = frame[1].X - frame[0].X, Y = frame[1].Y - frame[0].Y };
            Angle = (float)Vector.AngleBetween(a, b);
        }

        public static Line CreateLine(System.Drawing.PointF[] frame, System.Drawing.PointF coords)
        {
            Line l = new Line();
            l.Points[0] = frame[0];
            l.Points[1] = coords;
            l.CalculateAngle(frame);
            return l;
        }

    }
}
