﻿namespace RayCaster.N2942
{
    // Player class contains info for drawing a player(a circle) and keeping track of it's position's X and Y coordinates.
    public class Player
    {
        public int Radius { get; }
        public double PosX { get; set; }
        public double PosY { get; set; }

        public Player()
        {
            Radius = 25;
            PosX = 30;
            PosY = 30;
        }
    }
}
