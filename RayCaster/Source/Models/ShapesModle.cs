﻿using System.Collections.Generic;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;



namespace RayCaster.N2942
{
    //Shape class contains all structures and rays to be drawn. Also methods for they're calcualtions.
    public class Shapes
    {
        //Structures contains info for objects on map, everything is made of lines
        public List<Point[]> Structures { get; set; }

        //Rays contains info for drawing rays that shoot out from player position into every structure line
        public List<Point[]> Rays { get; set; }

        //Customline is used to allow player draw lines and create custom map
        public Point[] CustomLine { get; set; }

        public Shapes()
        {
            Structures = new List<Point[]>();
            Rays = new List<Point[]>();
            CustomLine = new Point[2];
        }

        //Method to read table from database and put every line in table into structures
        public void CreateStructures(string tableName)
        {
            List<Line> lines = Dbase.ReadFromSQLite(tableName);
            foreach (Line line in lines)
            {
                Point[] ps = new Point[2] { new Point { X = (int)line.Points[0].X, Y = (int)line.Points[0].Y }, new Point { X = (int)line.Points[1].X, Y = (int)line.Points[1].Y } };
                Structures.Add(ps);
            }
        }

        //Rays are drawn from player and into every line that is in structures
        public void CreateRays(int x, int y)
        {
            foreach (Point[] point in Structures)
            {
                Rays.Add(new Point[2] { new Point(x, y), point[0] });
                Rays.Add(new Point[2] { new Point(x, y), point[1] });
            }
        }

        //This method calulates intersection points between line segments( rays and structures). 4 points are given
        // as parameters which correspond to two lines, each having start and end point. Mathemathics for calculating
        // line segment intersections in 2D plane
        public PointF CalculateIntersection(Point p0, Point p1, Point p2, Point p3)
        {
            float a1 = p1.Y - p0.Y;
            float b1 = p0.X - p1.X;
            float c1 = (a1 * p0.X) + (b1 * p0.Y);

            float a2 = p3.Y - p2.Y;
            float b2 = p2.X - p3.X;
            float c2 = (a2 * p2.X) + (b2 * p2.Y);

            float denominator = a1 * b2 - a2 * b1;

            float secX = (b2 * c1 - b1 * c2) / denominator;
            float secY = (a1 * c2 - a2 * c1) / denominator;

            float segmentX1 = (secX - p0.X) / (p1.X - p0.X);
            float segmentY1 = (secY - p0.Y) / (p1.Y - p0.Y);
            float segmentX2 = (secX - p2.X) / (p3.X - p2.X);
            float segmentY2 = (secY - p2.Y) / (p3.Y - p2.Y);

            if (((segmentX1 >= 0 && segmentX1 <= 1) || (segmentY1 >= 0 && segmentY1 <= 1)) && ((segmentX2 >= 0 && segmentX2 <= 1) || (segmentY2 >= 0 && segmentY2 <= 1)))
            {
                PointF segmentIntersection = new PointF
                {
                    X = secX,
                    Y = secY
                };
                return segmentIntersection;
            }
            PointF intersection = new PointF
            {
                X = 5000,
                Y = 5000
            };
            return intersection;
        }

        //A single ray likely intersects with many structural lines on map, only the closest intersectionpoint is important
        //This method takes all intersection points and measures theyre distance from player, closest point is returned.
        public PointF CheckAllLines(List<Point[]> structures, Point[] line, float x, float y)
        {
            float distance = 10000;
            PointF f = new PointF
            {
                X = 5000,
                Y = 5000
            };

            foreach (Point[] structure in structures)
            {
                PointF point = CalculateIntersection(structure[0], structure[1], line[0], line[1]);
                if (point.Y != 5000)
                {
                    float a = point.X - x;
                    float b = point.Y - y;
                    float c = (float)Math.Sqrt(a * a + b * b);
                    if (c < distance)
                    {
                        distance = c;
                        f = point;
                    }
                }
            }
            return f;
        }

        //This method is used when user Clears map. Eventhough all lines are erased, edges of canvas are redrawn automatically.
        public void BasicFrame()
        {
            Structures.Add(new Point[] { new Point(0, 0), new Point(1023, 0) });
            Structures.Add(new Point[] { new Point(1023, 0), new Point(1023, 767) });
            Structures.Add(new Point[] { new Point(1023, 767), new Point(0, 767) });
            Structures.Add(new Point[] { new Point(0, 767), new Point(0, 0) });
        }

        //If a ray reaches it's intended endpoint on a line, two additional rays need to be sent at a small angle towards
        //same direction so that visibility polygon can be created. This method, using vectors, calculates direction point where new
        // rays have to be shot.
        public PointF TranslatePoint(PointF point, float offset, double x, double y)
        {
            PointF vector = new PointF()
            {
                X = point.X - (float)x,
                Y = point.Y - (float)y
            };

            float magnitude = (float)Math.Sqrt((vector.X * vector.X) + (vector.Y * vector.Y));
            vector.X /= magnitude;
            vector.Y /= magnitude;
            PointF translation = new PointF()
            {
                X = offset * vector.X,
                Y = offset * vector.Y
            };
            using (Matrix m = new Matrix())
            {
                m.Translate(translation.X, translation.Y);
                PointF[] pts = new PointF[] { point };
                m.TransformPoints(pts);
                return pts[0];
            }
        }

        //Previous method gives a point but it does not rotate it. A small tilt is need in clockwise and counterclockwise
        //direction to shoot rays behind corners. This method rotates a point by given angle. p1 is point to be rotated, p2
        //is point around which the rotation occurs
        public static Point[] RotateVector(PointF p1, PointF p2, double angle)
        {
            double radians = (Math.PI / 180) * angle;

            double sin = Math.Sin(radians);
            double cos = Math.Cos(radians);

            p1.X -= p2.X;
            p1.Y -= p2.Y;

            double xnew = p1.X * cos - p1.Y * sin;
            double ynew = p1.X * sin + p1.Y * cos;

            Point[] test = new Point[2];
            Point newPoint = new Point((int)Math.Round(xnew + p2.X), (int)Math.Round(ynew + p2.Y));
            test[0].X = (int)Math.Round(p2.X);
            test[0].Y = (int)Math.Round(p2.Y);
            test[1] = newPoint;

            return test;

        }

        //when all rays are calculated, to draw a polygon, intersection points have to be connected one by one.
        //this method sorts intersection base based on angel each line has relative to frame, so polygon is drawn in correct order
        public static int[] PolygonPoints(List<Line> lines)
        {
            lines.Sort((v, w) => v.Angle.CompareTo(w.Angle));
            List<int> polyPoints = new List<int>();
            foreach (Line l in lines)
            {
                polyPoints.Add((int)l.Points[1].X);
                polyPoints.Add((int)l.Points[1].Y);
            }
            polyPoints.Add(polyPoints[0]);
            polyPoints.Add(polyPoints[1]);
            int[] final = polyPoints.ToArray();

            return final;
        }
    }
}
