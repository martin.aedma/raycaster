﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace RayCaster.N2942
{

    public partial class MainWindow : Window
    {
        //Creatin Drawing-object that interacts between UI and Logic / Databases
        Drawing drawing = new Drawing();

        public MainWindow()
        {
            InitializeComponent();

            //Keyboard input
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(MovePlayer);
            timer.Start();

            //For databinding
            this.DataContext = drawing;

        }




        //setting Image source to Drawing-objects writeablebitmap
        private void ViewPort_Loaded(object sender, RoutedEventArgs e)
        {
            ViewPort.Source = drawing.BMPUser;


        }

        //Mouse left button eventhandlers to allow user to create custom map by drawing lines
        private void ViewPort_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if ((string)btnCreate.Content == "LOCK")
            {
                System.Windows.Point mPos = Mouse.GetPosition(ViewPort);
                drawing.MouseDown(mPos.X, mPos.Y);
            }
        }

        private void ViewPort_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((string)btnCreate.Content == "LOCK")
            {
                System.Windows.Point mPos = Mouse.GetPosition(ViewPort);
                System.Drawing.Point mouse = new System.Drawing.Point() { X = (int)Math.Round(mPos.X), Y = (int)Math.Round(mPos.Y) };
                drawing.user.CustomLine[0] = mouse;
            }
        }

        //Everything else is pretty self-explanatory, button names describe UI options. At the bottom is keyboard input.        
        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            if ((string)btnCreate.Content == "CREATE")
            {
                btnCreate.Content = "LOCK";
                btnRender.IsEnabled = false;
                btnSave.IsEnabled = true;
                btnLoad.IsEnabled = true;
                btnClearMap.IsEnabled = true;
            }
            else
            {
                btnCreate.Content = "CREATE";
                btnRender.IsEnabled = true;
                btnSave.IsEnabled = false;
                btnLoad.IsEnabled = false;
                btnClearMap.IsEnabled = false;
            }


        }

        private void btnRender_Click(object sender, RoutedEventArgs e)
        {
            if ((string)btnRender.Content == "RENDER")
            {
                CompositionTarget.Rendering += new EventHandler(drawing.RenderUser);
                btnRender.Content = "STOP";
                btnCreate.IsEnabled = false;
                btnRays.IsEnabled = true;
                btnPolygon.IsEnabled = true;
                btnShadows.IsEnabled = true;
            }
            else
            {
                CompositionTarget.Rendering -= drawing.RenderUser;
                btnRender.Content = "RENDER";
                btnCreate.IsEnabled = true;
                btnRays.IsEnabled = false;
                btnPolygon.IsEnabled = false;
                btnShadows.IsEnabled = false;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            drawing.SaveToDataBase();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            lstBoxMaps.ItemsSource = drawing.TableNames();
            stkPanelLoad.Visibility = Visibility.Visible;
            btnLoad.IsEnabled = false;
            btnSave.IsEnabled = false;
            btnCreate.IsEnabled = false;
            btnClearMap.IsEnabled = false;
        }

        private void btnListBoxLoad_Click(object sender, RoutedEventArgs e)
        {
            drawing.user.Structures.Clear();
            drawing.user.CreateStructures(lstBoxMaps.SelectedItem.ToString());
            stkPanelLoad.Visibility = Visibility.Hidden;
            EnableLoadButtons();
            System.EventArgs eventArgs = new EventArgs();
            drawing.RenderUser(drawing.user, eventArgs);
        }

        private void btnListBoxRemove_Click(object sender, RoutedEventArgs e)
        {
            string tableName = lstBoxMaps.SelectedItem.ToString();
            drawing.RemoveFromDataBase(tableName);
            lstBoxMaps.ItemsSource = drawing.TableNames();
        }

        private void btnListCancel_Click(object sender, RoutedEventArgs e)
        {
            EnableLoadButtons();
            stkPanelLoad.Visibility = Visibility.Hidden;
        }

        public void EnableLoadButtons()
        {
            btnLoad.IsEnabled = true;
            btnSave.IsEnabled = true;
            btnCreate.IsEnabled = true;
            btnClearMap.IsEnabled = true;
        }


        private void btnRays_Click(object sender, RoutedEventArgs e)
        {
            drawing.polygon = false;
            drawing.shadows = false;
        }

        private void btnPolygon_Click(object sender, RoutedEventArgs e)
        {
            drawing.polygon = true;
            drawing.shadows = false;
        }

        private void btnShadows_Click(object sender, RoutedEventArgs e)
        {
            drawing.polygon = false;
            drawing.shadows = true;
        }

        private void btnClearMap_Click(object sender, RoutedEventArgs e)
        {
            drawing.user.Structures.Clear();
            drawing.BMPUser.Clear();
            drawing.user.BasicFrame();

        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Confirm Porgram Exit", "Exit", MessageBoxButton.OKCancel);
            switch (result)
            {
                case MessageBoxResult.OK:
                    System.Windows.Application.Current.Shutdown();
                    break;
                case MessageBoxResult.Cancel:

                    break;
            }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            const string message = "RENDER - render's current Map. Default is a empty Map.\n" +
                "     Use keyboard buttons W, A, S ,D to move\n\n" +
                "RAYS / POLYGON / SHADOWS - are only available during rendering and switch\n" +
                "     between viewing rays, visibility polygon or line of sight\n\n" +
                "CREATE - allows to edit current map as follows:\n" +
                "   - User can draw lines on screen by pressing Mouse Left Button down\n" +
                "     then dragging mouse and releasing button to draw a line\n" +
                "   - SAVE saves the current Map with a generated name\n" +
                "   - LOAD user can load saved Maps. Inside loading menu user can also\n" +
                "     remove saved Maps\n" +
                "   - CLEAR MAP clears the map to blank canvas\n" +
                "   - LOCK When CREATE has been clicked, Map has to be locked before\n" +
                "     rendering is possible\n\n" +
                "SPEED - Rendering loop has unregulated Frames Per Second. Meaning it will\n" +
                "     have different running speed on different computers. If player figure\n" +
                "     moves too slow or fast, speed can be adjusted by PLUS and MINUS buttons\n" +
                "     Speed is pixels per button stroke and changes logarithmical_Loadely. Inital\n" +
                "     value is set to 0.01\n\n" +
                "INSTRUCTIONS - displays program info\n\n" +
                "EXIT - exits program";
            _ = MessageBox.Show(message, "Instructions", MessageBoxButton.OK);
        }

        private void btnSpeedPlus_Click(object sender, RoutedEventArgs e)
        {
            drawing.IncreaseSpeed();
        }

        private void btnSpeedMinus_Click(object sender, RoutedEventArgs e)
        {
            drawing.DecreaseSpeed();
        }

        private void MovePlayer(object sender, EventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.A))
            {
                if (drawing.player.PosX < 26)
                {
                    drawing.player.PosX -= 0;
                }
                else
                {
                    drawing.player.PosX -= drawing.Speed;
                }
            }
            if (Keyboard.IsKeyDown(Key.W))
            {
                if (drawing.player.PosY < 26)
                {
                    drawing.player.PosY -= 0;
                }
                else
                {
                    drawing.player.PosY -= drawing.Speed;
                }
            }
            if (Keyboard.IsKeyDown(Key.S))
            {
                if (drawing.player.PosY > 768 - 26)
                {
                    drawing.player.PosY -= 0;
                }
                else
                {
                    drawing.player.PosY += drawing.Speed;
                }
            }
            if (Keyboard.IsKeyDown(Key.D))
            {
                if (drawing.player.PosX > 1024 - 26)
                {
                    drawing.player.PosX += 0;
                }
                else
                {
                    drawing.player.PosX += drawing.Speed;
                }
            }
        }
    }
}

