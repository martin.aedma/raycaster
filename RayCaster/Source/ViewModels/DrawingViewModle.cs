﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.ComponentModel;

namespace RayCaster.N2942
{
    //Drawing class is intended as a "ViewModle". It interacts with all Modle classes and User Interface. User Interface
    //only interacts with Drawing class and Modle classes only interact with Drawing class.
    public class Drawing : INotifyPropertyChanged
    {
        public WriteableBitmap BMPUser { get; set; }
        public readonly Player player = new Player();
        public readonly Shapes user = new Shapes();
        public bool polygon { get; set; }
        public bool shadows { get; set; }
        private float speed;
        public float Speed
        {
            get
            {
                return speed;
            }

            set
            {
                if (value != speed)
                {
                    speed = value;
                    RaisePropertyChanged("Speed");
                    
                }
            }
        }

        //Constructor. Bitmap is initiated and map edges are set.
        public Drawing()
        {
            BMPUser = BitmapFactory.New(1024, 768);
            user.BasicFrame();
            polygon = false;
            shadows = false;
            speed = 0.01f;
            Speed = speed;

        }



        //This method could be called as the "Core" of this program. Everything that is drawn into image is done here.
        //Every calculation that has to be made is called from here and results are used for drawing lines on bitmap.
        //During Rendering this method is called rapidly.
        public void RenderUser(object sender, EventArgs e)
        {
            //player position is casted to integers, integers are used in Draw methods
            int x = (int)Math.Round(player.PosX);
            int y = (int)Math.Round(player.PosY);

            //frame is horizontal line from player to right edge of the map. This line is used to calculate angels for
            //each ray when polygon is drawn. Each ray is positioned at certain angel compared to this frame.
            System.Drawing.PointF[] frame = new System.Drawing.PointF[2];
            frame[0] = new System.Drawing.PointF() { X = (float)player.PosX, Y = (float)player.PosY };
            frame[1] = new System.Drawing.PointF() { X = 1023, Y = (float)player.PosY };

            //Previous rendered image and rays are cleared, ready to be calculated anew.
            WriteableBitmapExtensions.Clear(BMPUser);
            user.Rays.Clear();
            user.CreateRays(x, y);

            //list to collect all rays shooting out from player. Eventually this list is used to calculate polygon view
            List<Line> lines = new List<Line>();

            //each ray is calculated for every structure line in map.
            foreach (System.Drawing.Point[] ray in user.Rays)
            {
                //looking for intersections
                PointF p = user.CheckAllLines(user.Structures, ray, (float)player.PosX, (float)player.PosY);
                if (p.X < 1024)
                {
                    //ray is drawn from player to closest intersecting point
                    BMPUser.DrawLine((int)p.X, (int)p.Y, x, y, Colors.Black, null);
                    if (polygon != true)
                    {
                        BMPUser.FillEllipseCentered((int)p.X, (int)p.Y, 5, 5, Colors.MediumVioletRed);
                    }
                    Line line1 = Line.CreateLine(frame, p);
                    lines.Add(line1);

                    //if intersection point is particular rays intended endpoint (certain line ending) then two
                    //additional rays need to be casted in the same direction but with a small tilt on each side
                    if (ray[1].X == p.X && ray[1].Y == p.Y)
                    {
                        //new direction point is calculated
                        PointF p3 = user.TranslatePoint(p, 1280, player.PosX, player.PosY);

                        //that new point is rotated then by 1.05 and 358.95 (-1.05 counterclockwise) degrees clockwise
                        System.Drawing.Point[] pointsPlus = Shapes.RotateVector(p3, frame[0], 1.05);
                        System.Drawing.Point[] pointsMinus = Shapes.RotateVector(p3, frame[0], 358.95);

                        //once again intersections of those new rays are calculated and drawn. q1 is 1.05 degrees tilted ray
                        PointF q1 = user.CheckAllLines(user.Structures, pointsPlus, (float)player.PosX, (float)player.PosY);

                        if (q1.X < 1024)
                        {
                            BMPUser.DrawLine((int)Math.Round(q1.X), (int)Math.Round(q1.Y), x, y, Colors.Black, null);

                            if (polygon != true)
                            {
                                BMPUser.FillEllipseCentered((int)q1.X, (int)q1.Y, 5, 5, Colors.MediumVioletRed);
                            }
                            Line line2 = Line.CreateLine(frame, q1);
                            lines.Add(line2);
                        }

                        //q2 is -1.05 degrees tilted ray
                        PointF q2 = user.CheckAllLines(user.Structures, pointsMinus, (float)player.PosX, (float)player.PosY);

                        if (q2.X < 1024)
                        {
                            BMPUser.DrawLine((int)Math.Round(q2.X), (int)Math.Round(q2.Y), x, y, Colors.Black, null);
                            if (polygon != true)
                            {
                                BMPUser.FillEllipseCentered((int)q2.X, (int)q2.Y, 5, 5, Colors.MediumVioletRed);
                            }
                            Line line3 = Line.CreateLine(frame, q2);
                            lines.Add(line3);
                        }
                    }
                }
            }

            //if polygon is chosen by user, then here polygon points are ordered by angel and then polygon is drawn
            //if shadows is chosen, same polygon is drawn but "painters algorithm" is apllied, which means visibility polygon is drawn lastly
            if (polygon == true && shadows == false)
            {
                BMPUser.FillPolygon(Shapes.PolygonPoints(lines), Colors.PaleVioletRed);
            }
            else if (shadows == true && polygon == false)
            {
                BMPUser.FillRectangle(0, 0, 1023, 767, Colors.DimGray);
                BMPUser.FillPolygon(Shapes.PolygonPoints(lines), Colors.White);
            }

            //finally player and structures are drawn. This ends current frame.
            BMPUser.DrawEllipseCentered(x, y, player.Radius, player.Radius, Colors.Black);
            foreach (System.Drawing.Point[] point in user.Structures)
            {
                BMPUser.DrawLine(point[0].X, point[0].Y, point[1].X, point[1].Y, Colors.Black, null);
            }
        }

        //Methods used for Drawing-viewmodel to interact with database on behalf of User interface.
        public void SaveToDataBase()
        {
            Dbase.WriteToSqlite(user.Structures);
        }

        public void RemoveFromDataBase(string tableName)
        {
            Dbase.RemoveMap(tableName);
        }

        public List<string> TableNames()
        {
            List<string> tableNames = Dbase.GetTableNames();
            return tableNames;
        }

        //Helper method to remove some code and logic from mainwindow.cs and make UI "simpler"
        public void MouseDown(double mouseX, double mouseY)
        {
            System.Drawing.Point mouse = new System.Drawing.Point() { X = (int)Math.Round(mouseX), Y = (int)Math.Round(mouseY) };
            user.CustomLine[1] = mouse;
            BMPUser.DrawLine(user.CustomLine[0].X, user.CustomLine[0].Y, user.CustomLine[1].X, user.CustomLine[1].Y, Colors.Black, null);
            System.Drawing.Point[] temp = new System.Drawing.Point[2] { new System.Drawing.Point(user.CustomLine[0].X, user.CustomLine[0].Y), new System.Drawing.Point(user.CustomLine[1].X, user.CustomLine[1].Y) };
            user.Structures.Add(temp);
        }

        public void IncreaseSpeed()
        {
            if (Speed > 1)
            {
                return;
            }
            else
            {
                Speed = (float)Math.Round(Speed * 10, 4);
            }
        }

        public void DecreaseSpeed()
        {
            if (Speed < 0.001)
            {
                return;
            }
            else
            {
                Speed = (float)Math.Round(Speed / 10, 4);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
