# TTOS0300 Käyttöliittymien ohjelmointi harjoitustyo

* RayCaster
* N2942 Martin Aedma
* Kevät 2020

## Sisällysluettelo 

1. [Johdanto](#johdanto)
2. [Tekijästä](#tekij%C3%A4st%C3%A4)
3. [Sovelluksen yleiskuvaus](#sovelluksen-yleiskuvaus)
4. [Kohdeyleisö](#kohdeyleis%C3%B6)
5. [Käyttäjäroolit](#k%C3%A4ytt%C3%A4j%C3%A4roolit)
6. [Käyttöympäristö ja käytetyt teknologiat](#k%C3%A4ytt%C3%B6ymp%C3%A4rist%C3%B6-ja-k%C3%A4ytetyt-teknologiat)
7. [Toiminnalliset vaatimukset](#toiminnalliset-vaatimukset)
8. [Käyttötapaukset](#k%C3%A4ytt%C3%B6tapaukset)
9. [Käsitemalli](#k%C3%A4sitemalli)
10. [Luokkamalli](#luokkamalli)
11. [Työnjako](#ty%C3%B6njako)
12. [Aikataulu](#aikataulu)


## Johdanto

Tämä projekti on opiskelijan N2942 harjoitustyö opintojaksossa - TTOS0300 Käyttöliittymien ohjelmointi / kevät 2020.

## Tekijästä

N2942 on JAMK 1. vuoden ICT opiskelija, suuntautunut ohjelmistotekniikkaan. Koko ohjelman tekee N2942 (Martin Aedma).

## Sovelluksen yleiskuvaus

Raycasting on tietokone grafikassa ja visualisoinissa metodi millä etsitään tietoa jossakin suunnassa olevista objekteista 2D tai 3D ympäristössä. Esimerkiksi, tietokone peleissä joku ampuu luotin niin 
lähettämällä suoran (ray) ampumispisteestä edeenpäin voidaan havaita törmääkö ray(luoti) johonkin. Ensimmäiset FPS pelit kuten DOOM ja Castle Wolfenstein käyttivät RayCasting metodia 
grafikan piirtämiseen. Pelaajasta lähetetään suoria ulospäin ja piirretään maailma / seinat siten miten nämä suorat iskevät tiettyin pisteisiin.

Tässä sovelluksessa luodaan RayCasting esimerkki ns "top down" näkökulmasta. Ikkunassa liikku pelaaja jota voidaan ohjata näppäimistöllä, pelaajan näköala "line of sight" riippuu 
"asiosta" mitkä sijaitsevat pelikentällä. RayCasting käyttämllä ohjelma laske pelaajalle näkyvillä olevan alan.

![](raycasting.png)

Top Down View esimerkki

![](Shooter.png)

RayCaster:in tarkoitus on tässä tapauksessa piirtä ns "Visibility Polygon". Eli käyttäjä ei voi nähdä kartalla olevien objektien taakse. Käyttäjälle tarkoitettu näköalo lasketaan
ja tuloksena piirretään näköala polygoni.

![](visibility_polygon.png)

## Kohdeyleisö

Tietokone graafikasta ja sen ohjelmoinnista kiinnostuneet + tietokonella ajanviettettä haluavat henkilöt.

## Käyttäjäroolit

*  Käyttäjä toimii pelaajana

## Käyttöympäristö ja käytetyt teknologiat

*  Toteutus XAML- ja C# ohjelmointikieliä käyttäen 
*  WPF sovelluksena 
*  SQLite, WritetablebitmapEx ja System.Drawing kirjastot / NuGet paketit
*  Windows ympäristöön

## Toiminnalliset vaatimukset

| Vaatimus | Kuvaus |							
|:-:|:-:|
| RayCasting | Näytöllä sijaitsee pelaaja josta ulospäin ammutaan suoria mitkä leikkavat pelikentällä olevia objektteja tai pelikentän reunoja | 
| Movement | Käyttäjä pystyy liikumaan pelikentällä ja RayCasting tapahtuu reaaliajassa pelajaan liikumisen mukaan |
| Polygoni | Käyttäjä pystyy näkemään näköalan polygonina | 
| Map Editor | Käyttäjä pystyy tekemään oman kartan vetämällä viivoja pelikentälle | 
| Save/Load  | Karttoja voi tallentaa ja poistaa | 
| Instructions | Käyttäjä pystyy lukemaan ohjeet sovelluksessa | 

## Käyttötapaukset

```plantuml
:User: --> (Render Map)
(Render Map) --> (As Rays)
(Render Map) --> (As Polygon)

:User: --> (Create Map)
(Create Map) --> (Draw)
(Create Map) --> (Clear Canvas)
(Create Map) --> (Save / Load / Remove)
(Save / Load / Remove) --> (DataBase)
(DataBase) --> (Save / Load / Remove)

:User: --> (Instructions)
:User: --> (Exit)
```
## Käsitemalli

![](Kasitemalli.png)

## Luokkamalli

Luokkamalli on yleinen ensikuva, varmaan muuttuu ja lisäntyy paljon ohjelmoinnin aikana. Loppuraportissa sitten näkyy todellinen.

![](Classes.png)

* Käyttäjä keskustele sovelluksen kanssa antamalla input tietoja käyttöliittymän Buttoneiden ja näppäimistön kauttaa
* Sovellus laskee input perusteella tarvittavat tiedot kuvan piirtämiseksi
* Kun Render on kytketty päälle, sovellus piirtä kuvia reaaliajassa kuuntelemalla input signaaleja
* Käyttäjä voi piirtää oman kartan, tallentaa sen, avataa aikaisemmin tallennetuja karttoja tai poista niitä. Tätä varten käytetään tietokanta.
* Jokainen tallennettu kartta on oma taulu tietokannassa.

## Työnjako

Martin Aedma (N2942) toeuttaa koko projektin

## Aikataulu

* Projekti on valmista ja palautettu 27.4.2020
* Toiminnaliisuuksia lisätään mahdollisuuksien mukaan
* Toiminnalisuudet valmista viikko ennen palautusta
* 20.4-27.4 loppuhiomista ja loppuraportti
