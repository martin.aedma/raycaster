# N2942 RayCaster Loppuraportti

* TTOS0300 Käyttöliittymien ohjelmointi
* N2942 Martin Aedma
* Kevät 2020

## Sisällysluettelo 

1. [Asennus](#asennus)
2. [Tietoja ohjelmasta](#tietoja-ohjelmasta)
3. [Käyttöliittymä](#k%C3%A4ytt%C3%B6liittym%C3%A4)
4. [Tietokanta](#tietokanta)
5. [Bugit / Ongelmat](md#bugit-ongelmat)
6. [Kehitysideat](#kehitysideat)
7. [Haastet ja opittuu](#haastet-ja-opittuu)
8. [Luokkamalli ja MVVM malli](#luokkamalli-ja-mvvm-malli)
9. [Työnjako](#ty%C3%B6njako)
10. [Ehdotus Arvosanaksi](#ehdotus-arvosanaksi)

## Asennus

### .NET Framework:in ulkopuliset kirjastot

Sovelluksessa käytetään seuraavia .NET Framework:in ulkopuolisia kirjastoja (NuGet paketit):

* WriteableBitmapEx
* System.Drawing.Common
* System.Data.SQLite
* System.Data.SQLite.Core
* System.Data.SQLite.EF6
* System.Data.SQLite.Linq

Kaikki nämä kirjastot ovat sovelluksen mukana eli ei tarvii asenna niitä erikseen.

### Konfigurointi

Sovelluskansion latauksen jälkeen on tarvittava asettaa tietokannan osoite asetuksiin.
Tämä tapahtuu näin:

#### 1. Release kansion sisällä on "FinalProjectGUI.exe.config" niminen tiedosto. Avaa se text editor:illa -> hiiren oikea button ja open with ... (vaikka notepad)

![](config.png)

#### 2. Tiedoston loppuosassa löytyy paikka missä lukee <value> C:\Users\Ettur\source\repos\FinalProjectGUI\shapes.db </value> ,tämä osoite pitää vaihtaa.

![](editing.png)

#### 3. Realase kansion sisällä on "shapes.db" niminen SQLite tietokanta. Uusi osoite pitää osoittaa tämään tiedostoon. Tiedoston osoitteen pääset hakee kun klikkaat tiedostolle hiiren oikealla nappulalla ja valitset Properties. Pystyt sitten tekemään tiedoston osoitteelle copy.

![](location.png)

#### 4. Aseta uusi osoite tilalle ja varmista että osoitteen lopussa on \shapes.db (kun teet copy Location niin tämä ei sisältä tiedoston oma nimeä, se pitää asetta itse)

![](newlocation.png)

#### 5. Tallenna ja sulje "FinalProjectGUI.exe.config". Sovellus on käyttövalmis, avaa sovellus -> FinalProjectGUI.exe 

## Tietoja ohjelmasta

#### CompositionTarget.Rendering ja Frames Per Second

Vaikka WPF ei ole tarkoitettu reaaliaikaseen grafikaan esityksen, pystyin sovelluksen kuitenkin sen tekemään. Käyttämäni CompositionTarget.Rendering metodia ei pystyy rajoittamaan 
kuinka usein se laukeaa. Siksi se laukee tosi paljon ja kuormittaa CPU:tä enemmän kun olisi tarvettaa. Tästä johtuu myös se, että Rendering taphatuu eri nopeuksilla eri tietokoneilla.
Jotta nopeuutta voisi säätää on käytössä Speed toiminta, mitää käyttäjä voi muuttaa sovelluksessa tarpeen mukaan.

#### Toteutetut toiminnalliset vaatimukset:

| Vaatimus | Toteutus |							
|:-:|:-:|
| RayCasting | Toteutettu : Käyttäjästä "ammutaan" suoria kohti jokaista objekttia pelikentällä, leikkauspisteet objektien kanssa merkataan punasilla palleroilla | 
| Movement | Toteutettu : Käyttäjä pystyy liikumaan pelikentällä ja RayCasting tapahtuu reaaliajassa käyttäjän liikumisen mukaan |
| Polygoni | Toteutettu : Käyttäjä pystyy vaihtamaan erilaisten view mode välillä - polygon mukaanlukien | 
| Map Editor | Toteutettu : Käyttäjä pystyy tekemään oman kartan vetämällä viivoja pelikentälle | 
| Save/Load  | Toteutettu : Karttoja voi tallentaa ja poistaa. Tämä tapahtuu SQLite tietokannan kautta | 
| Instructions | Toteutettu : Käyttäjä pystyy lukemaan ohjeet sovelluksessa painaamalla INSTRUCTIONS painiketta | 

Kaikki asetetut toiminnalisuudet toteutettiin, lisäksi pari toiminnallisuutta mitkä toeutettiin yli alkuperäisten vaatimusten:

| Toiminnallisuus | Toteutus |							
|:-:|:-:|
| Shadows | Käyttäjä pystyy vaihtamaan kolmen eri viewmode välillä - Rays, Polygon ja Shadows. Viimeinen invertoi Polygon näkymän eli paikat, mitä käyttäjä näkee ovat valkoiset ja muut ovat varjoissa. Tämä näkymä antaa ns todellisen näköalan pelitilanteesta | 
| Speed | Johtuen CompositionTarget.Rendering metodista, ohjelma toimii eri nopeuksilla eri tietokoneilla. Lisämällä tai vähentämällä nopeuttaa päästään konfiguroimaan pelaajan liikumisen nopeuutta eri tietokoneilla. |


## Käyttöliittymä

#### Menu

Käyttäjälle tarkoitettu valikoima:

![](gui1.png)

Käyttäjällä on 2 yleisetä toimintaa mitä hän pystyy tekemään, jompa kumpa kerralla, ei molempia. RENDER tai CREATE , nämä nappulat ovat väritetyt.
Valitsemalla RENDER suljetaan CREATE nappula pois käytöstä, sama pätee toisinpäin.

#### RENDER

* RENDER - sovellus alkaa esittämään valittua karttaa. Näytöllä on pelaaja (ympyrä) , mitää pystyy liikuttamaan painamalla näppäimistöllä A, W, S, D painikeita. Reaaliajassa lasketaan ja piirretään näköala polygoni. Sovelluksen käynnistettyä, on oletuksena tyhjä kartta. RENDER lopetetaan painamalla STOP.
* RAYS / POLYGON / SHADOWS - vaihtaa näkymä. Rays view piirtää suoria ja niiden leikkauspisteitä objekttien kanssa. Polygon view yhtistää kaikki leikkauspisteet vaaleapunaiseksi näköala polygoniksi. Shadows vaihtaa tilanteen näin, että näköala muuttu valkoiseksi ja muut aluet menee varjoihin. 

#### CREATE

* CREATE avaa kartan editoinnin. Pelaaja voi muokata karttaa piirtämällää siihen viivoja. Painamalla hiiren vaseman nappulan alas laitetaan viivan alkupisten, liikutetaan hiirta ja nostetaan vasen painike ylös. Viiva piirretään. CREATE menusta pääse pois painaamalla LOCK.
* SAVE / LOAD / REMOVE karttoja voi tallentaa painaamlla SAVE - nimi generoitaan automaattisesti ja tallentaan SQLite tietokantaan. Painamalla LOAD haetaan tallennetut kartat (jokainen on oma taulu tietokannassa), käyttäjä pystyy lataaman karttoja tai poistamaan niitä painamalla REMOVE.
* CLEAR MAP - puhtistaa kartan. Nolla kaikki objektit. Kuitenkin, vaikka kartta on objekteista tyhjä, reunaviivat piirretään takaisin automaattisesti.

#### SPEED

Riipuen tietokonesta voi alussa pelaajan liikuminen näytöllä olla liian nopea, hitas tai ok. Mikäli tarvii muokata nopeuuttaa, sen pystyy tekee painaamalla PLUS tai MINUS.
Nopeus tarkoittaa kuinka monta pixelia pelaaja liikkuu painamalla näppäimistöä. Muutoksen ovat logaritmista, oletuksena 0.01.

#### INSTRUCTIONS

Sovelluksen tomintaa kuvataan käyttäjälle

#### Kuvakaappaukset

#### 1. RENDER -> RAYS

![](rays.png)

#### 2. RENDER -> POLYGON

![](polygon.png)

#### 3. RENDER -> SHADOWS

![](shadows.png)

## Tietokanta

Sovelluksessa käytetään SQLite tietokanta, nimeltään "shapes.db". Tämä tietokanta sijaitse Release kansiossa. Jokainen tauluu sisältää yhden kartan objekttien tiedot.
Kaikki objektit kartalla on tehty viivoista. Jokaisella viivalla on alkuupiste ja loppupiste. Jokaisella pisteella on X ja Y kortinaatit. Eli tietokannan jokainen rivi
sisältää objektin numeron ensimäisessä sarakkeessa (ID number 1,2,3 ... jne) ja sen jälkeen neljä sarakkeetta INT numeroilla - alkupiste X , alkupiste Y, loppupiste X, loppupiste Y.
Niillä tietoilla piirretään käyttäjälle tallennettu kartta tai tallennetaan uusi taulu tietokantaan.
Tietokanta osoite pitää kertoa sovellukselle, konfigurointi selostus löytyy ylhäältä.

![](sqlite.png)

![](lines.png)

## Bugit / Ongelmat

#### CompositionTarget.Rendering 

En pystyy rajoittamaan CompositionTarget.Rendering laukemista, pikaisen laskennan peruustella sanoisin että se laukeaa muutama tuhattaa kertaa sekunnissa.
Siksi sovellus käyttää Rendering aikana paljon enemmän tietokonen resursseja kun olis tarve. Riittäisi 60 kerta sekunnissa, eli 60 Frame Per Second.

#### Käyttäjä vetää viivoja

Käyttäjä pystyy rakentamaan oman kartan vetäämällä viivoja hiiren vasemalla nappulalla. Jostain syystä hiiren pointerin paikka näytöllä ja viiva näytöllä poikkea 
toisisitaan jonkin verran. Eli kun päästä hiiren nappulan ylös ja viiva ilmestyy, se ei ole ihan siinä missä pointeri vaan vähän muualla.

#### Integers vs Floating Points

Kaikki viivat WriteableBitmapEx vaattii INT numeroita ja siitä johtuen menetetään paljon tarkkuuttaa. Siksi kaikki viivat näytöllä on vähän "pixeloitu" eikä ihan sulavia.
Laskemissa käytetään floating point numeroita mutta lopuksi ne pyöristetään integereiksi.

## Kehitysideat

Mun oma pikku idea on harjoitella ohjelmointia yleisesti ja myös tietokone grafikan ohjelmointia rakentamalla semmonen top down view shooter peli. Ydimessa pelaajaa liikkuu samalla lailla kun sovelluksessakin, muttaa joutuu tuhomaan pahiksia 
eri aseilla. Rakennan pelia C++ käyttäen, SFML (small fast media layer) kirjaston kanssa mikä on hyvä ympäristo siihen. Kun tähän opintojakson oli tehtävä harjoitustyö niin halusin 
tehdä RayCasterin kun se liitty mun oman projektiin. Oli erittäin hyvä oppimista tämän rakentaminen, sekä tietokonen grafikan osalta että WPF sovelluksista.
Jatkaan pelin kehitystä, mahdollisesti siis lopullinen peli tulee jonkun muun opintojakson harjoitustyöksi.

## Haastet ja opittuu

Todella iso asia RayCastein kanssa oli oppia ja ymmärtää tarvittavaa matematiikka. Suorien segmenttien leikkauspisteiten laskeminen on ohjelman ydin, tykkäsin tosi paljon sen 
oppimisesta ja olin / olen erittäin tyytyväinen kun sain sen onnistumaan. Muistan kun rakensin leikkauspisteiden rakentamisen funktiota ja sitten kun se rupees oikeasti toimii niin 
olin erittäin iloinen :) Suorien lisäksi tuli opittuu vektoreiden käsittelyä jonkin verran, vektorit ja 2D tasolla pisteiden rotaatiota. Varmasti otan matematikan opintojakson 
vektorit ja maatritsit syksyllä. Ne on tietokonegrafikassa olennaisia asioita.

Käyttöliittymän puolella tunti itselle olevan asia aika selkeä ja suoraviivaista, WPF kontrollit - tuli käytetty suurin piirtein kaikkia mitä oli myös käytiin läpi opintojaksolla.
Tapahtumankäsittelijat ja databinding onnistui myös. MVVM mallin toeutus oli haastavin niistä, ainakin tämän sovelluksen näkökulmasta, en ole varma kuinka hyvin se meni.

## Luokkamalli ja MVVM malli

MVVM mallin toeutus tuntui minulle haastavin osa kaäyttöliitymän puolelta. Voi olla myös että sovelluksen tyyppi vaikuttaa siihen.

Rakensin luokat MVVM mallia ajatellen näin: 

![](classes2.png)

* Player, Shapes, Line ja Database ovat Model luokkia
* Drawing on tarkoitettu ViewModel luokkana, mikä käyttää Model luokkia ja User Interface inputia laskemisen, piirtämisen ja tietokanta toimiinnoihin. Model luokat ja UI ei kommunikoi keskenään suoraan.
* User Interface on tarkoitettu View luokkana

## Työnjako

Martin Aedma (N2942) toteutti koko projektin.

## Ehdotus Arvosanaksi

About kaikkea mitä opintojaksolla opetettiin toteutin sovelluksessa:

* WPF kontrollit - StackPanel, DockPanel, Button, Label, ListBox, Image
* Tapahtumankäsittelijät (hiiri, näppäimistö, _Loaded jne)
* DataBinding ja INotifyPropertyChanged - ei tullut kyllä paljon niitä, vain yksi :) , mutta se riippuu myös sovelluksesta. Löysin paikan (Speed Label) mihin sijoittaa One Way Binding ja INotifyPropertyChanged.
* Tietokanta - on yhtistetty sovelluksen käyttöön
* User Interface - koitin tehdä siistin ja minimaalisen, hyvännäköisen ja helposti käytettävän
* Dokumentaatio luulen että on ok

Tein paljon töitä sovelluksen ohjelmoinnissa ja käytin opintojakson materiaaleja. Vaikka aiheen valinta ei ollut paras WPF sovellusta ajattelleen mutta oman subjektiivisen tunteen mukaan laittaisin vitosen :)