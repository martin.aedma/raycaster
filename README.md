# RayCaster

## Course project for GUI programming
It was obligatory to make WPF application in C#

I decided to combine my interest in Graphics Programming and linear algebra / mathematics and make a raycaster

* Author: Martin Aedma, N2942
* Subject: RayCaster as a WPF application written in C#
* Spring 2020

## Contents:
* [Specs](Docs/specs.md)
* [Report](Docs/Report.md)
* [Code](RayCaster/Source/)
* [Video](https://www.youtube.com/watch?v=vQf141AZHdM)

Information (Specs, Report) is written in Finnish as was required in course
